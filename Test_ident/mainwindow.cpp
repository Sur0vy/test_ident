#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QRandomGenerator>
#include <QIntValidator>
#include <QStringListModel>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->leValue->setValidator(new QIntValidator(0, 10000000));
    ui->leBeginErr->setValidator(new QIntValidator(0, 10000000));
    ui->leMiddleErr->setValidator(new QIntValidator(0, 10000000));
    ui->leEndErr->setValidator(new QIntValidator(0, 10000000));
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::isPeriodic(uint32_t *data, uint32_t size)
{
    double dif = 0.1; /*10% poeriod dif*/
    uint32_t i;

    if (size < 2)
        return false;
    const uint32_t period_size = size - 1;
    uint32_t period = 0;
    uint32_t min_period = data[period_size] - data[0];
    uint32_t max_period = 0;
    for (i = 0; i < period_size; i++)
    {
        period = data[i + 1] - data[i];
        if (min_period > period)
            min_period = period;
        else if (max_period < period)
            max_period = period;
    }
    return ((dif - (static_cast<double>(max_period) / static_cast<double>(min_period) - 1.0)) > 0) ? true : false;
}


void MainWindow::on_pbGenerate_clicked()
{
    delete array;

    QRandomGenerator *generator = QRandomGenerator::global();

    uint32_t val = static_cast<uint32_t>(ui->leValue->text().toInt());

    uint32_t size1 = static_cast<uint32_t>(ui->sbBegin->value());
    uint32_t size2 = static_cast<uint32_t>(ui->sbMiddle->value());
    uint32_t size3 = static_cast<uint32_t>(ui->sbEnd->value());
    uint32_t i = 0;

    arrSize = size1 + size2 + size3;
    array = new uint32_t[arrSize];
    uint32_t reperVal = 0;

    uint32_t err = static_cast<uint32_t>(ui->leBeginErr->text().toInt());
    uint32_t max = val + (err * val / 100);
    uint32_t min = val - (err * val / 100);
    for (i = 0; i < size1; ++i) {
        reperVal += static_cast<uint32_t>(generator->bounded(min, max));
        array[i] = reperVal;
    }

    err = static_cast<uint32_t>(ui->leMiddleErr->text().toInt());
    max = val + (err * val / 100);
    min = val - (err * val / 100);
    for (i = size1; i < size1 + size2; ++i){
        reperVal += static_cast<uint32_t>(generator->bounded(min, max));
        array[i] = reperVal;
    }

    err = static_cast<uint32_t>(ui->leEndErr->text().toInt());
    max = val + (err * val / 100);
    min = val - (err * val / 100);
    for (i = size1 + size2; i < arrSize; ++i){
        reperVal += static_cast<uint32_t>(generator->bounded(min, max));
        array[i] = reperVal;
    }
    showVals();
    ui->sbFirst->setRange(1, static_cast<int32_t>(arrSize));
    ui->sbLast->setRange(1, static_cast<int32_t>(arrSize));
    ui->sbLast->setValue(static_cast<int32_t>(arrSize));
}

void MainWindow::on_pbAnalyze_clicked()
{
    uint32_t *tmpArr;
    uint32_t tmpArrSize = static_cast<uint32_t>(ui->sbLast->value() - ui->sbFirst->value() + 1);
    tmpArr = new uint32_t[tmpArrSize];
    int j = 0;

    QStringListModel *model = new QStringListModel(this);
    QStringList list;

    list.append("Analyzing array:");
    for (int i = ui->sbFirst->value(); i <= ui->sbLast->value(); ++i) {
        tmpArr[j++] = array[i - 1];
        list.append(QString::number(array[i - 1]));
    }

    if (isPeriodic(tmpArr, tmpArrSize)) {
        list.append("Signal is periodic");
    } else {
        list.append("Signal is not periodic");
    }

    model->setStringList(list);
    ui->lvRes->setModel(model);
}

void MainWindow::showVals()
{
    QStringListModel *model = new QStringListModel(this);
    QStringList list;
    for (uint32_t i = 0; i < arrSize; ++i)
        list.append(QString::number(array[i]));
    model->setStringList(list);
    ui->lvValues->setModel(model);

}

void MainWindow::on_pbLeftStep_clicked()
{
    ui->sbFirst->setValue(ui->sbFirst->value() - 1);
    ui->sbLast->setValue(ui->sbLast->value() - 1);
}

void MainWindow::on_pbRightStep_clicked()
{
    ui->sbFirst->setValue(ui->sbFirst->value() + 1);
    ui->sbLast->setValue(ui->sbLast->value() + 1);
}

void MainWindow::on_pbAutoAnalyze_clicked()
{
    const uint32_t cnt = 10;
    uint32_t first = 0;
    int j = 0;
    QStringListModel *model = new QStringListModel(this);
    QStringList list;

    uint32_t tmpArrSize = cnt;
    uint32_t *tmpArr = new uint32_t[tmpArrSize];

    bool stop = false;
    list.append("Analyzing array:");
    while (!stop) {
        j = 0;
        for (uint32_t i = first; i < tmpArrSize + first; ++i) {
            tmpArr[j++] = array[i];
        }
        stop = isPeriodic(tmpArr, tmpArrSize);
        if (stop) {
            list.append("periodic from: ");
            list.append("index = " + QString::number(first) +
                        " value = " + QString::number(array[first]) +
                        " array size = " + QString::number(tmpArrSize));
        }
        first++;
    }
    model->setStringList(list);
    ui->lvRes->setModel(model);
    if (arrSize <= tmpArrSize + first) {
        stop = true;
        list.append("Signal is not periodic");
        list.append("End of array.");
    }
}
