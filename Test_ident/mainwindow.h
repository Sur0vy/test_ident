#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pbGenerate_clicked();
    void on_pbAnalyze_clicked();
    void on_pbLeftStep_clicked();
    void on_pbRightStep_clicked();
    void on_pbAutoAnalyze_clicked();

private:
    Ui::MainWindow *ui;
    uint32_t *array;
    uint32_t arrSize;
    static bool isPeriodic(uint32_t *data, uint32_t size);
    void showVals();
};
#endif // MAINWINDOW_H
